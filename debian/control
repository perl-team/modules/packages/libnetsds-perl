Source: libnetsds-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libcache-memcached-fast-perl <!nocheck>,
                     libcgi-fast-perl <!nocheck>,
                     libclass-accessor-class-perl <!nocheck>,
                     libclass-accessor-perl <!nocheck>,
                     libclass-errorhandler-perl <!nocheck>,
                     libconfig-general-perl <!nocheck>,
                     libdata-structure-util-perl <!nocheck>,
                     libdbd-pg-perl <!nocheck>,
                     libdbi-perl <!nocheck>,
                     libfcgi-perl <!nocheck>,
                     libhtml-template-pro-perl <!nocheck>,
                     libjson-perl <!nocheck>,
                     libjson-xs-perl <!nocheck>,
                     liblocale-gettext-perl <!nocheck>,
                     libnet-server-mail-perl <!nocheck>,
                     libnetsds-util-perl <!nocheck>,
                     libproc-daemon-perl <!nocheck>,
                     libproc-pid-file-perl <!nocheck>,
                     libtest-pod-coverage-perl <!nocheck>,
                     libtest-pod-perl <!nocheck>,
                     libunix-syslog-perl <!nocheck>
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libnetsds-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libnetsds-perl.git
Homepage: https://metacpan.org/release/NetSDS
Rules-Requires-Root: no

Package: libnetsds-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libcache-memcached-fast-perl,
         libcgi-fast-perl,
         libclass-accessor-class-perl,
         libclass-accessor-perl,
         libclass-errorhandler-perl,
         libconfig-general-perl,
         libdata-structure-util-perl,
         libdbd-pg-perl,
         libdbi-perl,
         libfcgi-perl,
         libhtml-template-pro-perl,
         libjson-perl,
         libjson-xs-perl,
         liblocale-gettext-perl,
         libnet-server-mail-perl,
         libnetsds-util-perl,
         libproc-daemon-perl,
         libproc-pid-file-perl,
         libunix-syslog-perl
Description: Service Delivery Suite framework
 NetSDS is a flexible framework for rapid software development using the
 following technologies:
 .
  * Perl5 - default programming language
  * PostgreSQL - default DBMS
  * MemcacheQ - message queue manager
  * Apache - HTTP server with FastCGI support
  * Kannel - SMS and WAP gateway
  * Asterisk - VoIP / telephony applications
  * Mbuni - MMSC and MMS VAS gateway
